--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Debian 10.5-1.pgdg90+1)
-- Dumped by pg_dump version 10.5

-- Started on 2018-10-31 08:32:40

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE digitaledition;
--
-- TOC entry 3178 (class 1262 OID 16385)
-- Name: digitaledition; Type: DATABASE; Schema: -; Owner: root
--

CREATE DATABASE digitaledition WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE digitaledition OWNER TO root;

\connect digitaledition

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12980)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3181 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pg_sQL procedural language';


--
-- TOC entry 196 (class 1259 OID 16386)
-- Name: contributor_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.contributor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contributor_seq OWNER TO root;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 16388)
-- Name: contributor; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.contributor (
    id bigint DEFAULT nextval('public.contributor_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.144759'::timestamp without time zone,
    date_modified timestamp without time zone,
    publication_collection_id bigint,
    publication_collection_introduction_id bigint,
    publication_collection_title_id bigint,
    publication_id bigint,
    publication_manuscript_id bigint,
    publication_version_id bigint,
    deleted smallint DEFAULT 0,
    type text,
    first_name text,
    last_name text,
    description text,
    prefix text
);


ALTER TABLE public.contributor OWNER TO root;

--
-- TOC entry 198 (class 1259 OID 16397)
-- Name: event_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.event_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_seq OWNER TO root;

--
-- TOC entry 199 (class 1259 OID 16399)
-- Name: event; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.event (
    id bigint DEFAULT nextval('public.event_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.223099'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    type text,
    description text
);


ALTER TABLE public.event OWNER TO root;

--
-- TOC entry 200 (class 1259 OID 16408)
-- Name: event_connection_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.event_connection_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_connection_seq OWNER TO root;

--
-- TOC entry 203 (class 1259 OID 16414)
-- Name: event_connection; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.event_connection (
    id bigint DEFAULT nextval('public.event_connection_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.528531'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    subject_id bigint,
    tag_id bigint,
    location_id bigint,
    event_id bigint
);


ALTER TABLE public.event_connection OWNER TO root;

--
-- TOC entry 201 (class 1259 OID 16410)
-- Name: event_occurrence_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.event_occurrence_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_occurrence_seq OWNER TO root;

--
-- TOC entry 204 (class 1259 OID 16420)
-- Name: event_occurrence; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.event_occurrence (
    id bigint DEFAULT nextval('public.event_occurrence_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.658718'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    type text,
    description text,
    event_id bigint,
    publication_id bigint,
    publication_version_id bigint,
    publication_manuscript_id bigint,
    publication_facsimile_id bigint,
    publication_comment_id bigint,
    publication_facsimile_page bigint
);


ALTER TABLE public.event_occurrence OWNER TO root;

--
-- TOC entry 202 (class 1259 OID 16412)
-- Name: event_relation_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.event_relation_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.event_relation_seq OWNER TO root;

--
-- TOC entry 205 (class 1259 OID 16429)
-- Name: event_relation; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.event_relation (
    id bigint DEFAULT nextval('public.event_relation_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.729647'::timestamp without time zone,
    date_modified timestamp without time zone,
    event_id bigint,
    related_event_id bigint,
    deleted smallint DEFAULT 0
);


ALTER TABLE public.event_relation OWNER TO root;

--
-- TOC entry 206 (class 1259 OID 16435)
-- Name: location_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.location_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.location_seq OWNER TO root;

--
-- TOC entry 207 (class 1259 OID 16437)
-- Name: location; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.location (
    id bigint DEFAULT nextval('public.location_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.303296'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    name text,
    country text,
    city text,
    description text,
    legacy_id text,
    latitude numeric(9,6),
    longitude numeric(9,6),
    project_id bigint,
    region text,
    source text
);


ALTER TABLE public.location OWNER TO root;

--
-- TOC entry 208 (class 1259 OID 16446)
-- Name: project_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.project_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.project_seq OWNER TO root;

--
-- TOC entry 209 (class 1259 OID 16448)
-- Name: project; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.project (
    id bigint DEFAULT nextval('public.project_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:27.577202'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    published bigint,
    name text
);


ALTER TABLE public.project OWNER TO root;

--
-- TOC entry 210 (class 1259 OID 16457)
-- Name: publication_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.publication_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publication_seq OWNER TO root;

--
-- TOC entry 211 (class 1259 OID 16459)
-- Name: publication; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.publication (
    id bigint DEFAULT nextval('public.publication_seq'::regclass) NOT NULL,
    publication_collection_id bigint,
    publication_comment_id bigint,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:27.733419'::timestamp without time zone,
    date_modified timestamp without time zone,
    date_published_externally timestamp without time zone,
    deleted smallint DEFAULT 0,
    published bigint,
    legacy_id text,
    published_by text,
    original_filename text,
    name text,
    genre text,
    publication_group_id bigint,
    original_publication_date timestamp without time zone
);


ALTER TABLE public.publication OWNER TO root;

--
-- TOC entry 214 (class 1259 OID 16472)
-- Name: publication_collection_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.publication_collection_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publication_collection_seq OWNER TO root;

--
-- TOC entry 222 (class 1259 OID 16488)
-- Name: publication_collection; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.publication_collection (
    id bigint DEFAULT nextval('public.publication_collection_seq'::regclass) NOT NULL,
    publication_collection_introduction_id bigint,
    publication_collection_title_id bigint,
    project_id bigint,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:27.652586'::timestamp without time zone,
    date_modified timestamp without time zone,
    date_published_externally timestamp without time zone,
    deleted smallint DEFAULT 0,
    published bigint,
    name text
);


ALTER TABLE public.publication_collection OWNER TO root;

--
-- TOC entry 212 (class 1259 OID 16468)
-- Name: publication_collection_intro_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.publication_collection_intro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publication_collection_intro_seq OWNER TO root;

--
-- TOC entry 223 (class 1259 OID 16497)
-- Name: publication_collection_introduction; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.publication_collection_introduction (
    id bigint DEFAULT nextval('public.publication_collection_intro_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:27.410734'::timestamp without time zone,
    date_modified timestamp without time zone,
    date_published_externally timestamp without time zone,
    deleted smallint DEFAULT 0,
    published bigint,
    legacy_id text,
    original_filename text
);


ALTER TABLE public.publication_collection_introduction OWNER TO root;

--
-- TOC entry 213 (class 1259 OID 16470)
-- Name: publication_collection_title_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.publication_collection_title_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publication_collection_title_seq OWNER TO root;

--
-- TOC entry 224 (class 1259 OID 16506)
-- Name: publication_collection_title; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.publication_collection_title (
    id bigint DEFAULT nextval('public.publication_collection_title_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:27.501043'::timestamp without time zone,
    date_modified timestamp without time zone,
    date_published_externally timestamp without time zone,
    deleted smallint DEFAULT 0,
    published bigint,
    legacy_id text,
    original_filename text
);


ALTER TABLE public.publication_collection_title OWNER TO root;

--
-- TOC entry 215 (class 1259 OID 16474)
-- Name: publication_comment_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.publication_comment_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publication_comment_seq OWNER TO root;

--
-- TOC entry 225 (class 1259 OID 16515)
-- Name: publication_comment; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.publication_comment (
    id bigint DEFAULT nextval('public.publication_comment_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:27.900523'::timestamp without time zone,
    date_modified timestamp without time zone,
    date_published_externally timestamp without time zone,
    deleted smallint DEFAULT 0,
    published bigint,
    legacy_id text,
    published_by text,
    original_filename text
);


ALTER TABLE public.publication_comment OWNER TO root;

--
-- TOC entry 218 (class 1259 OID 16480)
-- Name: publication_facsimile_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.publication_facsimile_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publication_facsimile_seq OWNER TO root;

--
-- TOC entry 226 (class 1259 OID 16524)
-- Name: publication_facsimile; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.publication_facsimile (
    id bigint DEFAULT nextval('public.publication_facsimile_seq'::regclass) NOT NULL,
    publication_facsimile_collection_id bigint,
    publication_id bigint,
    publication_manuscript_id bigint,
    publication_version_id bigint,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.630247'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    page_nr integer NOT NULL,
    section_id bigint NOT NULL,
    priority bigint NOT NULL,
    type bigint NOT NULL
);


ALTER TABLE public.publication_facsimile OWNER TO root;

--
-- TOC entry 216 (class 1259 OID 16476)
-- Name: publication_facsimile_collec_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.publication_facsimile_collec_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publication_facsimile_collec_seq OWNER TO root;

--
-- TOC entry 227 (class 1259 OID 16530)
-- Name: publication_facsimile_collection; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.publication_facsimile_collection (
    id bigint DEFAULT nextval('public.publication_facsimile_collec_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.558943'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    title text,
    number_of_pages bigint,
    start_page_number bigint,
    description text,
    folder_path text,
    page_comment text,
    "external_uRL" text
);


ALTER TABLE public.publication_facsimile_collection OWNER TO root;

--
-- TOC entry 217 (class 1259 OID 16478)
-- Name: publication_facsimile_zone_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.publication_facsimile_zone_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publication_facsimile_zone_seq OWNER TO root;

--
-- TOC entry 228 (class 1259 OID 16539)
-- Name: publication_facsimile_zone; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.publication_facsimile_zone (
    id bigint DEFAULT nextval('public.publication_facsimile_zone_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.758617'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    publication_facsimile_id bigint
);


ALTER TABLE public.publication_facsimile_zone OWNER TO root;

--
-- TOC entry 219 (class 1259 OID 16482)
-- Name: publication_group_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.publication_group_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publication_group_seq OWNER TO root;

--
-- TOC entry 229 (class 1259 OID 16545)
-- Name: publication_group; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.publication_group (
    id bigint DEFAULT nextval('public.publication_group_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:27.977918'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    published smallint DEFAULT 0,
    name text
);


ALTER TABLE public.publication_group OWNER TO root;

--
-- TOC entry 220 (class 1259 OID 16484)
-- Name: publication_manuscript_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.publication_manuscript_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publication_manuscript_seq OWNER TO root;

--
-- TOC entry 230 (class 1259 OID 16555)
-- Name: publication_manuscript; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.publication_manuscript (
    id bigint DEFAULT nextval('public.publication_manuscript_seq'::regclass) NOT NULL,
    publication_id bigint,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.05304'::timestamp without time zone,
    date_modified timestamp without time zone,
    date_published_externally timestamp without time zone,
    deleted smallint DEFAULT 0,
    published bigint,
    legacy_id text,
    published_by text,
    original_filename text,
    name text,
    type bigint,
    section_id bigint,
    sort_order bigint
);


ALTER TABLE public.publication_manuscript OWNER TO root;

--
-- TOC entry 221 (class 1259 OID 16486)
-- Name: publication_version_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.publication_version_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.publication_version_seq OWNER TO root;

--
-- TOC entry 231 (class 1259 OID 16564)
-- Name: publication_version; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.publication_version (
    id bigint DEFAULT nextval('public.publication_version_seq'::regclass) NOT NULL,
    publication_id bigint,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:27.808947'::timestamp without time zone,
    date_modified timestamp without time zone,
    date_published_externally timestamp without time zone,
    deleted smallint DEFAULT 0,
    published bigint,
    legacy_id text,
    published_by text,
    original_filename text,
    name text,
    type integer,
    section_id bigint,
    sort_order bigint
);


ALTER TABLE public.publication_version OWNER TO root;

--
-- TOC entry 232 (class 1259 OID 16573)
-- Name: review_comment_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.review_comment_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.review_comment_seq OWNER TO root;

--
-- TOC entry 233 (class 1259 OID 16575)
-- Name: review_comment; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.review_comment (
    id bigint DEFAULT nextval('public.review_comment_seq'::regclass) NOT NULL,
    publication_id bigint,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.800318'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    comment text,
    "user" text,
    legacy_id text,
    review_comment_id bigint
);


ALTER TABLE public.review_comment OWNER TO root;

--
-- TOC entry 234 (class 1259 OID 16584)
-- Name: subject_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.subject_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subject_seq OWNER TO root;

--
-- TOC entry 235 (class 1259 OID 16586)
-- Name: subject; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.subject (
    id bigint DEFAULT nextval('public.subject_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.378104'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    type text,
    first_name text,
    last_name text,
    place_of_birth text,
    occupation text,
    preposition text,
    full_name text,
    description text,
    legacy_id text,
    date_born timestamp without time zone,
    date_deceased timestamp without time zone,
    project_id bigint,
    source text
);


ALTER TABLE public.subject OWNER TO root;

--
-- TOC entry 236 (class 1259 OID 16595)
-- Name: tag_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.tag_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tag_seq OWNER TO root;

--
-- TOC entry 237 (class 1259 OID 16597)
-- Name: tag; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.tag (
    id bigint DEFAULT nextval('public.tag_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.461819'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    type text,
    name text,
    description text,
    legacy_id text,
    project_id bigint,
    source text
);


ALTER TABLE public.tag OWNER TO root;

--
-- TOC entry 238 (class 1259 OID 16606)
-- Name: urn_lookup_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.urn_lookup_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.urn_lookup_seq OWNER TO root;

--
-- TOC entry 239 (class 1259 OID 16608)
-- Name: urn_lookup; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.urn_lookup (
    id bigint DEFAULT nextval('public.urn_lookup_seq'::regclass) NOT NULL,
    date_created timestamp without time zone DEFAULT '2018-10-12 09:12:28.862527'::timestamp without time zone,
    date_modified timestamp without time zone,
    deleted smallint DEFAULT 0,
    urn text,
    url text,
    reference_text text,
    intro_reference_text text,
    project_id bigint,
    legacy_id text
);


ALTER TABLE public.urn_lookup OWNER TO root;

--
-- TOC entry 2979 (class 2606 OID 16696)
-- Name: publication_collection_introduction PRIMARY; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_collection_introduction
    ADD CONSTRAINT "PRIMARY" PRIMARY KEY (id);


--
-- TOC entry 2981 (class 2606 OID 16698)
-- Name: publication_collection_title PRIMARY1; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_collection_title
    ADD CONSTRAINT "PRIMARY1" PRIMARY KEY (id);


--
-- TOC entry 2945 (class 2606 OID 16700)
-- Name: event PRIMARY10; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event
    ADD CONSTRAINT "PRIMARY10" PRIMARY KEY (id);


--
-- TOC entry 2964 (class 2606 OID 16702)
-- Name: location PRIMARY11; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT "PRIMARY11" PRIMARY KEY (id);


--
-- TOC entry 3008 (class 2606 OID 16704)
-- Name: subject PRIMARY12; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT "PRIMARY12" PRIMARY KEY (id);


--
-- TOC entry 3011 (class 2606 OID 16706)
-- Name: tag PRIMARY13; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT "PRIMARY13" PRIMARY KEY (id);


--
-- TOC entry 2947 (class 2606 OID 16708)
-- Name: event_connection PRIMARY14; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_connection
    ADD CONSTRAINT "PRIMARY14" PRIMARY KEY (id);


--
-- TOC entry 2991 (class 2606 OID 16710)
-- Name: publication_facsimile_collection PRIMARY15; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_facsimile_collection
    ADD CONSTRAINT "PRIMARY15" PRIMARY KEY (id);


--
-- TOC entry 2985 (class 2606 OID 16712)
-- Name: publication_facsimile PRIMARY16; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_facsimile
    ADD CONSTRAINT "PRIMARY16" PRIMARY KEY (id);


--
-- TOC entry 2953 (class 2606 OID 16714)
-- Name: event_occurrence PRIMARY17; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_occurrence
    ADD CONSTRAINT "PRIMARY17" PRIMARY KEY (id);


--
-- TOC entry 2961 (class 2606 OID 16716)
-- Name: event_relation PRIMARY18; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_relation
    ADD CONSTRAINT "PRIMARY18" PRIMARY KEY (id);


--
-- TOC entry 2993 (class 2606 OID 16718)
-- Name: publication_facsimile_zone PRIMARY19; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_facsimile_zone
    ADD CONSTRAINT "PRIMARY19" PRIMARY KEY (id);


--
-- TOC entry 2967 (class 2606 OID 16720)
-- Name: project PRIMARY2; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT "PRIMARY2" PRIMARY KEY (id);


--
-- TOC entry 3004 (class 2606 OID 16722)
-- Name: review_comment PRIMARY20; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.review_comment
    ADD CONSTRAINT "PRIMARY20" PRIMARY KEY (id);


--
-- TOC entry 3014 (class 2606 OID 16724)
-- Name: urn_lookup PRIMARY21; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.urn_lookup
    ADD CONSTRAINT "PRIMARY21" PRIMARY KEY (id);


--
-- TOC entry 2974 (class 2606 OID 16726)
-- Name: publication_collection PRIMARY3; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_collection
    ADD CONSTRAINT "PRIMARY3" PRIMARY KEY (id);


--
-- TOC entry 2969 (class 2606 OID 16728)
-- Name: publication PRIMARY4; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication
    ADD CONSTRAINT "PRIMARY4" PRIMARY KEY (id);


--
-- TOC entry 3001 (class 2606 OID 16730)
-- Name: publication_version PRIMARY5; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_version
    ADD CONSTRAINT "PRIMARY5" PRIMARY KEY (id);


--
-- TOC entry 2983 (class 2606 OID 16732)
-- Name: publication_comment PRIMARY6; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_comment
    ADD CONSTRAINT "PRIMARY6" PRIMARY KEY (id);


--
-- TOC entry 2996 (class 2606 OID 16734)
-- Name: publication_group PRIMARY7; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_group
    ADD CONSTRAINT "PRIMARY7" PRIMARY KEY (id);


--
-- TOC entry 2998 (class 2606 OID 16736)
-- Name: publication_manuscript PRIMARY8; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_manuscript
    ADD CONSTRAINT "PRIMARY8" PRIMARY KEY (id);


--
-- TOC entry 2937 (class 2606 OID 16738)
-- Name: contributor PRIMARY9; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.contributor
    ADD CONSTRAINT "PRIMARY9" PRIMARY KEY (id);


--
-- TOC entry 2986 (class 1259 OID 16739)
-- Name: facs_id; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX facs_id ON public.publication_facsimile USING btree (publication_facsimile_collection_id);


--
-- TOC entry 2938 (class 1259 OID 16742)
-- Name: fk_contributor_publication_collection_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_contributor_publication_collection_id_idx ON public.contributor USING btree (publication_collection_id);


--
-- TOC entry 2939 (class 1259 OID 16740)
-- Name: fk_contributor_publication_collection_introduction_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_contributor_publication_collection_introduction_id_idx ON public.contributor USING btree (publication_collection_introduction_id);


--
-- TOC entry 2940 (class 1259 OID 16741)
-- Name: fk_contributor_publication_collection_title_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_contributor_publication_collection_title_id_idx ON public.contributor USING btree (publication_collection_title_id);


--
-- TOC entry 2941 (class 1259 OID 16745)
-- Name: fk_contributor_publication_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_contributor_publication_id_idx ON public.contributor USING btree (publication_id);


--
-- TOC entry 2942 (class 1259 OID 16743)
-- Name: fk_contributor_publication_manuscript_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_contributor_publication_manuscript_id_idx ON public.contributor USING btree (publication_manuscript_id);


--
-- TOC entry 2943 (class 1259 OID 16744)
-- Name: fk_contributor_publication_version_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_contributor_publication_version_id_idx ON public.contributor USING btree (publication_version_id);


--
-- TOC entry 2948 (class 1259 OID 16746)
-- Name: fk_event_connection_event_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_event_connection_event_id_idx ON public.event_connection USING btree (event_id);


--
-- TOC entry 2949 (class 1259 OID 16747)
-- Name: fk_event_connection_location_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_event_connection_location_id_idx ON public.event_connection USING btree (location_id);


--
-- TOC entry 2950 (class 1259 OID 16748)
-- Name: fk_event_connection_subject_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_event_connection_subject_id_idx ON public.event_connection USING btree (subject_id);


--
-- TOC entry 2951 (class 1259 OID 16749)
-- Name: fk_event_connection_tag_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_event_connection_tag_id_idx ON public.event_connection USING btree (tag_id);


--
-- TOC entry 2954 (class 1259 OID 16750)
-- Name: fk_event_occurrence_event_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_event_occurrence_event_id_idx ON public.event_occurrence USING btree (event_id);


--
-- TOC entry 2955 (class 1259 OID 16751)
-- Name: fk_event_occurrence_publication_comment_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_event_occurrence_publication_comment_id_idx ON public.event_occurrence USING btree (publication_comment_id);


--
-- TOC entry 2956 (class 1259 OID 16752)
-- Name: fk_event_occurrence_publication_facsimile_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_event_occurrence_publication_facsimile_id_idx ON public.event_occurrence USING btree (publication_facsimile_id);


--
-- TOC entry 2957 (class 1259 OID 16755)
-- Name: fk_event_occurrence_publication_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_event_occurrence_publication_id_idx ON public.event_occurrence USING btree (publication_id);


--
-- TOC entry 2958 (class 1259 OID 16753)
-- Name: fk_event_occurrence_publication_manuscript_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_event_occurrence_publication_manuscript_id_idx ON public.event_occurrence USING btree (publication_manuscript_id);


--
-- TOC entry 2959 (class 1259 OID 16754)
-- Name: fk_event_occurrence_publication_version_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_event_occurrence_publication_version_id_idx ON public.event_occurrence USING btree (publication_version_id);


--
-- TOC entry 2962 (class 1259 OID 16756)
-- Name: fk_event_relation_event_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_event_relation_event_id_idx ON public.event_relation USING btree (event_id);


--
-- TOC entry 2965 (class 1259 OID 16757)
-- Name: fk_location_project_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_location_project_id_idx ON public.location USING btree (project_id);


--
-- TOC entry 2975 (class 1259 OID 16758)
-- Name: fk_publication_collection_project_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_publication_collection_project_id_idx ON public.publication_collection USING btree (project_id);


--
-- TOC entry 2976 (class 1259 OID 16759)
-- Name: fk_publication_collection_publication_collection_intro_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_publication_collection_publication_collection_intro_id_idx ON public.publication_collection USING btree (publication_collection_introduction_id);


--
-- TOC entry 2977 (class 1259 OID 16760)
-- Name: fk_publication_collection_publication_collection_title_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_publication_collection_publication_collection_title_id_idx ON public.publication_collection USING btree (publication_collection_title_id);


--
-- TOC entry 2987 (class 1259 OID 16764)
-- Name: fk_publication_facsimile_publication_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_publication_facsimile_publication_id_idx ON public.publication_facsimile USING btree (publication_id);


--
-- TOC entry 2988 (class 1259 OID 16762)
-- Name: fk_publication_facsimile_publication_manuscript_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_publication_facsimile_publication_manuscript_id_idx ON public.publication_facsimile USING btree (publication_manuscript_id);


--
-- TOC entry 2989 (class 1259 OID 16763)
-- Name: fk_publication_facsimile_publication_version_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_publication_facsimile_publication_version_id_idx ON public.publication_facsimile USING btree (publication_version_id);


--
-- TOC entry 2994 (class 1259 OID 16761)
-- Name: fk_publication_facsimile_zone_publication_facsimile_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_publication_facsimile_zone_publication_facsimile_id_idx ON public.publication_facsimile_zone USING btree (publication_facsimile_id);


--
-- TOC entry 2999 (class 1259 OID 16765)
-- Name: fk_publication_manuscript_publication_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_publication_manuscript_publication_id_idx ON public.publication_manuscript USING btree (publication_id);


--
-- TOC entry 2970 (class 1259 OID 16767)
-- Name: fk_publication_publication_collection_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_publication_publication_collection_id_idx ON public.publication USING btree (publication_collection_id);


--
-- TOC entry 2971 (class 1259 OID 16768)
-- Name: fk_publication_publication_comment_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_publication_publication_comment_id_idx ON public.publication USING btree (publication_comment_id);


--
-- TOC entry 2972 (class 1259 OID 16769)
-- Name: fk_publication_publication_group_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_publication_publication_group_id_idx ON public.publication USING btree (publication_group_id);


--
-- TOC entry 3002 (class 1259 OID 16766)
-- Name: fk_publication_version_publication_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_publication_version_publication_id_idx ON public.publication_version USING btree (publication_id);


--
-- TOC entry 3005 (class 1259 OID 16770)
-- Name: fk_review_comment_publication_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_review_comment_publication_id_idx ON public.review_comment USING btree (publication_id);


--
-- TOC entry 3006 (class 1259 OID 16771)
-- Name: fk_review_comment_review_comment_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_review_comment_review_comment_id_idx ON public.review_comment USING btree (review_comment_id);


--
-- TOC entry 3009 (class 1259 OID 16772)
-- Name: fk_subject_project_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_subject_project_id_idx ON public.subject USING btree (project_id);


--
-- TOC entry 3012 (class 1259 OID 16773)
-- Name: fk_tag_project_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_tag_project_id_idx ON public.tag USING btree (project_id);


--
-- TOC entry 3015 (class 1259 OID 16774)
-- Name: fk_urn_project_id_idx; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX fk_urn_project_id_idx ON public.urn_lookup USING btree (project_id);


--
-- TOC entry 3018 (class 2606 OID 16785)
-- Name: contributor fk_contributor_publication_collection_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.contributor
    ADD CONSTRAINT fk_contributor_publication_collection_id FOREIGN KEY (publication_collection_id) REFERENCES public.publication_collection(id);


--
-- TOC entry 3016 (class 2606 OID 16775)
-- Name: contributor fk_contributor_publication_collection_introduction_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.contributor
    ADD CONSTRAINT fk_contributor_publication_collection_introduction_id FOREIGN KEY (publication_collection_introduction_id) REFERENCES public.publication_collection_introduction(id);


--
-- TOC entry 3017 (class 2606 OID 16780)
-- Name: contributor fk_contributor_publication_collection_title_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.contributor
    ADD CONSTRAINT fk_contributor_publication_collection_title_id FOREIGN KEY (publication_collection_title_id) REFERENCES public.publication_collection_title(id);


--
-- TOC entry 3021 (class 2606 OID 16800)
-- Name: contributor fk_contributor_publication_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.contributor
    ADD CONSTRAINT fk_contributor_publication_id FOREIGN KEY (publication_id) REFERENCES public.publication(id);


--
-- TOC entry 3019 (class 2606 OID 16790)
-- Name: contributor fk_contributor_publication_manuscript_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.contributor
    ADD CONSTRAINT fk_contributor_publication_manuscript_id FOREIGN KEY (publication_manuscript_id) REFERENCES public.publication_manuscript(id);


--
-- TOC entry 3020 (class 2606 OID 16795)
-- Name: contributor fk_contributor_publication_version_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.contributor
    ADD CONSTRAINT fk_contributor_publication_version_id FOREIGN KEY (publication_version_id) REFERENCES public.publication_version(id);


--
-- TOC entry 3022 (class 2606 OID 16805)
-- Name: event_connection fk_event_connection_event_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_connection
    ADD CONSTRAINT fk_event_connection_event_id FOREIGN KEY (event_id) REFERENCES public.event(id);


--
-- TOC entry 3023 (class 2606 OID 16810)
-- Name: event_connection fk_event_connection_location_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_connection
    ADD CONSTRAINT fk_event_connection_location_id FOREIGN KEY (location_id) REFERENCES public.location(id);


--
-- TOC entry 3024 (class 2606 OID 16815)
-- Name: event_connection fk_event_connection_subject_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_connection
    ADD CONSTRAINT fk_event_connection_subject_id FOREIGN KEY (subject_id) REFERENCES public.subject(id);


--
-- TOC entry 3025 (class 2606 OID 16820)
-- Name: event_connection fk_event_connection_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_connection
    ADD CONSTRAINT fk_event_connection_tag_id FOREIGN KEY (tag_id) REFERENCES public.tag(id);


--
-- TOC entry 3026 (class 2606 OID 16825)
-- Name: event_occurrence fk_event_occurrence_event_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_occurrence
    ADD CONSTRAINT fk_event_occurrence_event_id FOREIGN KEY (event_id) REFERENCES public.event(id);


--
-- TOC entry 3027 (class 2606 OID 16830)
-- Name: event_occurrence fk_event_occurrence_publication_comment_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_occurrence
    ADD CONSTRAINT fk_event_occurrence_publication_comment_id FOREIGN KEY (publication_comment_id) REFERENCES public.publication_comment(id);


--
-- TOC entry 3028 (class 2606 OID 16835)
-- Name: event_occurrence fk_event_occurrence_publication_facsimile_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_occurrence
    ADD CONSTRAINT fk_event_occurrence_publication_facsimile_id FOREIGN KEY (publication_facsimile_id) REFERENCES public.publication_facsimile(id);


--
-- TOC entry 3031 (class 2606 OID 16850)
-- Name: event_occurrence fk_event_occurrence_publication_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_occurrence
    ADD CONSTRAINT fk_event_occurrence_publication_id FOREIGN KEY (publication_id) REFERENCES public.publication(id);


--
-- TOC entry 3029 (class 2606 OID 16840)
-- Name: event_occurrence fk_event_occurrence_publication_manuscript_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_occurrence
    ADD CONSTRAINT fk_event_occurrence_publication_manuscript_id FOREIGN KEY (publication_manuscript_id) REFERENCES public.publication_manuscript(id);


--
-- TOC entry 3030 (class 2606 OID 16845)
-- Name: event_occurrence fk_event_occurrence_publication_version_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_occurrence
    ADD CONSTRAINT fk_event_occurrence_publication_version_id FOREIGN KEY (publication_version_id) REFERENCES public.publication_version(id);


--
-- TOC entry 3032 (class 2606 OID 16855)
-- Name: event_relation fk_event_relation_event_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.event_relation
    ADD CONSTRAINT fk_event_relation_event_id FOREIGN KEY (event_id) REFERENCES public.event(id);


--
-- TOC entry 3033 (class 2606 OID 16860)
-- Name: location fk_location_project_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT fk_location_project_id FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- TOC entry 3037 (class 2606 OID 16865)
-- Name: publication_collection fk_publication_collection_project_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_collection
    ADD CONSTRAINT fk_publication_collection_project_id FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- TOC entry 3038 (class 2606 OID 16870)
-- Name: publication_collection fk_publication_collection_publication_collection_intro_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_collection
    ADD CONSTRAINT fk_publication_collection_publication_collection_intro_id FOREIGN KEY (publication_collection_introduction_id) REFERENCES public.publication_collection_introduction(id);


--
-- TOC entry 3039 (class 2606 OID 16875)
-- Name: publication_collection fk_publication_collection_publication_collection_title_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_collection
    ADD CONSTRAINT fk_publication_collection_publication_collection_title_id FOREIGN KEY (publication_collection_title_id) REFERENCES public.publication_collection_title(id);


--
-- TOC entry 3040 (class 2606 OID 16885)
-- Name: publication_facsimile fk_publication_facsimile_publication_facsimile_collection_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_facsimile
    ADD CONSTRAINT fk_publication_facsimile_publication_facsimile_collection_id FOREIGN KEY (publication_facsimile_collection_id) REFERENCES public.publication_facsimile_collection(id);


--
-- TOC entry 3043 (class 2606 OID 16900)
-- Name: publication_facsimile fk_publication_facsimile_publication_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_facsimile
    ADD CONSTRAINT fk_publication_facsimile_publication_id FOREIGN KEY (publication_id) REFERENCES public.publication(id);


--
-- TOC entry 3041 (class 2606 OID 16890)
-- Name: publication_facsimile fk_publication_facsimile_publication_manuscript_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_facsimile
    ADD CONSTRAINT fk_publication_facsimile_publication_manuscript_id FOREIGN KEY (publication_manuscript_id) REFERENCES public.publication_manuscript(id);


--
-- TOC entry 3042 (class 2606 OID 16895)
-- Name: publication_facsimile fk_publication_facsimile_publication_version_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_facsimile
    ADD CONSTRAINT fk_publication_facsimile_publication_version_id FOREIGN KEY (publication_version_id) REFERENCES public.publication_version(id);


--
-- TOC entry 3044 (class 2606 OID 16880)
-- Name: publication_facsimile_zone fk_publication_facsimile_zone_publication_facsimile_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_facsimile_zone
    ADD CONSTRAINT fk_publication_facsimile_zone_publication_facsimile_id FOREIGN KEY (publication_facsimile_id) REFERENCES public.publication_facsimile(id);


--
-- TOC entry 3045 (class 2606 OID 16905)
-- Name: publication_manuscript fk_publication_manuscript_publication_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_manuscript
    ADD CONSTRAINT fk_publication_manuscript_publication_id FOREIGN KEY (publication_id) REFERENCES public.publication(id);


--
-- TOC entry 3034 (class 2606 OID 16915)
-- Name: publication fk_publication_publication_collection_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication
    ADD CONSTRAINT fk_publication_publication_collection_id FOREIGN KEY (publication_collection_id) REFERENCES public.publication_collection(id);


--
-- TOC entry 3035 (class 2606 OID 16920)
-- Name: publication fk_publication_publication_comment_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication
    ADD CONSTRAINT fk_publication_publication_comment_id FOREIGN KEY (publication_comment_id) REFERENCES public.publication_comment(id);


--
-- TOC entry 3036 (class 2606 OID 16925)
-- Name: publication fk_publication_publication_group_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication
    ADD CONSTRAINT fk_publication_publication_group_id FOREIGN KEY (publication_group_id) REFERENCES public.publication_group(id);


--
-- TOC entry 3046 (class 2606 OID 16910)
-- Name: publication_version fk_publication_version_publication_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.publication_version
    ADD CONSTRAINT fk_publication_version_publication_id FOREIGN KEY (publication_id) REFERENCES public.publication(id);


--
-- TOC entry 3047 (class 2606 OID 16930)
-- Name: review_comment fk_review_comment_publication_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.review_comment
    ADD CONSTRAINT fk_review_comment_publication_id FOREIGN KEY (publication_id) REFERENCES public.publication(id);


--
-- TOC entry 3048 (class 2606 OID 16935)
-- Name: review_comment fk_review_comment_review_comment_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.review_comment
    ADD CONSTRAINT fk_review_comment_review_comment_id FOREIGN KEY (review_comment_id) REFERENCES public.review_comment(id);


--
-- TOC entry 3049 (class 2606 OID 16940)
-- Name: subject fk_subject_project_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.subject
    ADD CONSTRAINT fk_subject_project_id FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- TOC entry 3050 (class 2606 OID 16945)
-- Name: tag fk_tag_project_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT fk_tag_project_id FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- TOC entry 3051 (class 2606 OID 16950)
-- Name: urn_lookup fk_urn_project_id; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.urn_lookup
    ADD CONSTRAINT fk_urn_project_id FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- TOC entry 3180 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: root
--

GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2018-10-31 08:32:41

--
-- PostgreSQL database dump complete
--

